class Game {
    constructor (player1, computer) {
        this.player1 = player1
        this.computer = computer
        this.isCanPlay =  false
    }

    getRandomValue () {
        const num = Math.floor(Math.random() * 3);
        let choosen = '';
        this.computer = choosen;

        if (num === 0) {
            guntingCOM.changeBackgorundColor()
            return choosen = 'gunting'
        } else if (num === 1) {
            batuCOM.changeBackgorundColor()
            return choosen = 'batu'
        } else if (num === 2) {
            kertasCOM.changeBackgorundColor()
            return choosen = 'kertas'
            
        }

        // this.computer = choosen
        // console.log(choosen, '<<< ini getRandom')
        return choosen
        
        // return choosen
        // console.log(`COM memilih ${choosen}`);
    }

    getResult () {
        // console.log(player1, '<<ini player1')
        // console.log (computer, '<< ini com')
        if (player1.choosen === 'kertas' && computer.choosen === 'batu') {
            document.getElementById("versus").innerHTML = '<span class = "jsclass-p1win"> PLAYER 1 WIN</span>'
        } else if (player1.choosen === 'kertas' && computer.choosen === 'gunting') {
            document.getElementById("versus").innerHTML = '<span class = "jsclass-comwin";> COM WIN </span>'
        } else if (player1.choosen === 'kertas' && computer.choosen === 'kertas') {
            document.getElementById("versus").innerHTML = '<span class = "jsclass-draw"> DRAW </span>'
        } else if (player1.choosen === 'gunting' && computer.choosen === 'batu') {
            document.getElementById("versus").innerHTML = '<span class = "jsclass-comwin";> COM WIN </span>'
        } else if (player1.choosen === 'gunting' && computer.choosen === 'kertas') {
            document.getElementById("versus").innerHTML = '<span class = "jsclass-p1win"> PLAYER 1 WIN</span>'
        } else if (player1.choosen === 'gunting' && computer.choosen === 'gunting') {
            document.getElementById("versus").innerHTML = '<span class = "jsclass-draw"> DRAW </span>'
        } else if (player1.choosen === 'batu' && computer.choosen === 'kertas') {
            document.getElementById("versus").innerHTML = '<span class = "jsclass-comwin";> COM WIN </span>'
        } else if (player1.choosen === 'batu' && computer.choosen === 'gunting') {
            document.getElementById("versus").innerHTML = '<span class = "jsclass-p1win"> PLAYER 1 WIN</span>'
        } else {
            document.getElementById("versus").innerHTML = '<span class = "jsclass-draw"> DRAW </span>'
        }
    }
}

class Player {
    constructor (user, choosen){
        this.user = user
        this.choosen = choosen
        this.game = new Game();
    }
    
    chooseCard (user) {
        this.choosen = user; 
        console.log(`Player memilih ${user}`);
    }

    chooseCard2 () {
        this.choosen = this.game.getRandomValue()
        console.log(`COM memilih ${this.choosen}`)
    }
}

class Card {
    constructor (type, isChoosen, element, isClick) {
        this.type = type
        this.isChoosen = isChoosen
        this.player = new Player ()
        this.element = element
        this.isClick = isClick

        if(this.isClick){
            this.element.addEventListener('click', () => {
                this.changeBackgorundColor()
            })
        }
    }

    changeBackgorundColor () {
        this.element.style.backgroundColor = '#C4C4C4'
        this.element.style.width = '223px'
        this.element.style.height = '204px'
        this.element.style.borderRadius = '30px'
        this.element.style.paddingTop = '30px'
        this.element.style.paddingLeft = '50px'
    }
}

const batu1 = document.getElementById('batu-1');
const kertas1 = document.getElementById('kertas-1');
const gunting1 = document.getElementById('gunting-1');

const batuComputer = document.getElementById('batu-COM');
const kertasComputer = document.getElementById('kertas-COM');
const guntingComputer = document.getElementById('gunting-COM');


const player1 = new Player ('player 1', null);
const computer = new Player ('com', null);

const game = new Game (player1, computer);

const guntingP1 = new Card ('Gunting', false, gunting1,true)
const batuP1 = new Card ('Batu', false, batu1,true)
const kertasP1 = new Card ('Kertas', false, kertas1, true)

const guntingCOM = new Card ('Gunting', false, guntingComputer)
const batuCOM = new Card ('Batu', false, batuComputer)
const kertasCOM = new Card ('Kertas', false, kertasComputer)

const p1 = {
    name : 'Player 1',
    choosen : ''
}

const com = {
    name : '',
    choosen : ''
}

//Ngehubungin card dengan player
gunting1.addEventListener('click', function(){
    player1.chooseCard('gunting');
    computer.chooseCard2();
    game.getResult();
    // guntingP1.changeBackgorundColor();
    // guntingCOM.changeBackgorundColorCOM();
});

batu1.addEventListener('click', function(){
    player1.chooseCard('batu');
    computer.chooseCard2();
    game.getResult();
    // batuP1.changeBackgorundColor();
    // batuCOM.changeBackgorundColorCOM();
});

kertas1.addEventListener('click', function(){
    player1.chooseCard('kertas');
    computer.chooseCard2();
    game.getResult();
    // kertasP1.changeBackgorundColor();
    // kertasCOM.changeBackgorundColorCOM();
});
